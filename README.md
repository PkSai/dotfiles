# Dotfiles

These are configuration files for various applications running on the Linux/Unix operating systems, this repository consists of configuration files for a couple of terminal emulators, a window manager; namely the i3 window manager along with its bar, i3status, the notification manager dunst as well as the compositor picom. If you're thinking of using these configuration files I would suggest you edit these files first to suit your workflow environment. Updating for.


![image](https://gitlab.com/PkSai/dotfiles/-/raw/master/screenshots/ss1.png)


## Application List
### Terminal emulators:
	* Alacritty
	* termite

### Terminal monitors:
	* Bashtop
	* htop

### Window Manager:
	* i3wm
	* i3status(Bar)

### Filemanager :
	* nnn
	* ranger

### Miscellaneous
	* compton	
	* picom
	* dunst
	* conky
	* calcurse

### Vim:
	* 'VundleVim/Vundle.vim'
	* 'scrooloose/nerdtree'
	* 'jistr/vim-nerdtree-tabs'
	* 'cocopon/iceberg.vim'
	* 'morhetz/gruvbox'
	* 'nerdypepper/agila.vim'
	* 'xuhdev/vim-latex-live-preview'
	* 'lervag/vimtex'
	* 'junegunn/goyo.vim'
	* 'altercation/vim-colors-solarized'


## More Screenshots

![image](https://gitlab.com/PkSai/dotfiles/-/raw/master/screenshots/ss2.png)

![image](https://gitlab.com/PkSai/dotfiles/-/raw/master/screenshots/ss3.png)


## Installation

```bash
git clone https://gitlab.com/PkSai/dotfiles.git
```


