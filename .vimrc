"If you ever want to make use of a leader key
let mapleader=","


"-------FILE HANDLING-------------
"Automatic spell checking for tex and markdown files
autocmd FileType tex,latex.markdown,md setlocal spell spelllang=en_us 
"This is to remove auto commenting for the next line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o


" ------Key bindings ------
nnoremap <leader>c :make %:r<CR>
nnoremap <leader>j :!java %<CR>
nnoremap <C-g> :Goyo<CR>
vnoremap <C-c> "+y
nnoremap <leader>l :LLPStartPreview<CR>
nnoremap <leader>L :!pdflatex %<CR>
" Split navigation
map <C-LEFT> <C-w><LEFT>
map <C-RIGHT> <C-w><RIGHT>
map <C-UP> <C-w><UP>
map <C-DOWN> <C-w><DOWN>
"Shortcut for splits
nnoremap <leader>h :split
nnoremap <leader>v :vsplit
"Alias for write and quit
nnoremap <leader>q :wq!<CR>
nnoremap <leader>w :w<CR>
"This is to remap escape as ctrl-L for faster mode change
imap <C-L> <Esc>
" Vim's auto indentation feature does not work properly with text copied from outisde of Vim. Press the <F2>
" key to toggle paste mode on/off.
nnoremap <F2> :set invpaste paste?<CR>
imap <F2> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F2>

"--------- PLUGINS -----------------------
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
	Plugin 'VundleVim/Vundle.vim'
	Plugin 'scrooloose/nerdtree'
	Plugin 'jistr/vim-nerdtree-tabs'
	Plugin 'cocopon/iceberg.vim'
	Plugin 'morhetz/gruvbox'
	Plugin 'nerdypepper/agila.vim'
	Plugin 'xuhdev/vim-latex-live-preview'
	Plugin 'lervag/vimtex'
	Plugin 'junegunn/goyo.vim'
	Plugin 'altercation/vim-colors-solarized'
call vundle#end()            " required

"---------- SOME BASIC SETTINGS -------------
" Set compatibility to Vim only.
set nocompatible
" Helps force plug-ins to load correctly when it is turned back on below.
filetype plugin on 
" Turn on syntax highlighting.
syntax on
syntax enable
" For plug-ins to load correctly.
filetype plugin indent on
" The set bacground is to get the dark gruvbox colorscheme
set background=dark
colorscheme iceberg 
" Turn off modelines
set modelines=0
"Does not make backups
set nobackup
"Mouse/Touchpad functionality
set mouse=
set ttymouse=
" Automatically wrap text that extends beyond the screen length.
set wrap
" Uncomment below to set the max textwidth. Use a value corresponding to the width of your screen.
" set textwidth=79
set formatoptions=tcqrn1
set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab
set noshiftround
" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start
" Speed up scrolling in Vim
set ttyfast
"Goyo width
let g:goyo_width = 120
let g:goyo_height = 90
" Status bar
set laststatus=2
" Display options
set showmode
set showcmd
" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>
" Show line numbers
set number
" Set status line display
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ [BUFFER=%n]\ %{strftime('%c')}
" Encoding
set encoding=utf-8
""Toggle
map <C-o> :NERDTreeToggle<CR>
" Highlight matching search patterns
set hlsearch
" Enable incremental search
set incsearch
" Include matching uppercase words with lowercase search term
set ignorecase
" Include only uppercase words with uppercase search term
set smartcase
" Store info from no more than 100 files at a time, 9999 lines of text, 100kb of data. Useful for copying lar
" ge amounts of data between files.
set viminfo='100,<9999,s100
" Map the <Space> key to toggle a selected fold opened/closed.
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf
" Automatically save and load folds
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview"
" Call the .vimrc.plug file
 if filereadable(expand("~/.vimrc.plug"))
     source ~/.vimrc.plug
 endif
"YCM thing
let g:ycm_server_keep_logfiles = 1
let g:ycm_server_log_level = 'debug'
" Removing insert
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'iceberg',
      \ }
" Copy pasting usig xclip
vmap <leader>C :!xclip -f -sel clip<CR>
"---------FOR PROGRAMMING ----------
set exrc
set secure
set colorcolumn=110
highlight ColorColumn ctermbg=darkgray
let &path.="src/include,/usr/include/AL,"
set relativenumber
":+13 to jump to line 13, -13...
set cursorline
"Autocompletion
set wildmode=longest,list,full
"vim-tex pdf viewer
let g:livepreview_previewer = 'mupdf'
" Fix Alacritty mouse
set ttymouse=sgr
autocmd FileType tex inoremap :sec \section{}<Enter><Enter><Esc><2kf}i
